// import "./App.css";
import React, { Component } from "react";
//-------------------------------------------------------
import users from "./data/users";
//-------------------------------------------------------
import User from "./components/User";
import TableHeaders from "./components/TableHeaders";
//=======================================================

class App extends Component {
  render() {
    return (
      <div className="App container-fluid ">
        <h1 className="my-5 text-center"> User Information </h1>
        <div className="mx-5">
          <table className="table table-striped">
            <thead>
              <TableHeaders
                heads={[
                  "Avatar",
                  "Full Name",
                  "Age",
                  "Location",
                  "Username",
                  "Email",
                  "Phone",
                ]}
              />
            </thead>
            <tbody>
              {users.results.map((user, ind) => (
                <User {...user} indx={ind} />
              ))}
            </tbody>
          </table>
        </div>
        <div className="my-5 text-center">
          <br />
          <span> page: {users.info.page}</span>
          <span> results: {users.info.results}</span>
        </div>
      </div>
    );
  }
}

export default App;
