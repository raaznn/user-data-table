import React, { Component } from "react";

export default class TableHeaders extends Component {
  render() {
    return (
      <tr>
        <th scope="col">#</th>
        {this.props.heads.map((head) => {
          return <th scope="col">{head}</th>;
        })}
      </tr>
    );
  }
}
