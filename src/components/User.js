import React, { Component } from "react";

export default class User extends Component {
  render() {
    return (
      <tr>
        <th scope="row" className="align-middle">
          {this.props.indx + 1}
        </th>
        <td className="align-middle">
          <img
            style={{ borderRadius: "50%" }}
            src={this.props.picture.thumbnail}
          />
        </td>
        <td className="align-middle">
          {this.props.name.title} {this.props.name.first} {this.props.name.last}
        </td>
        <td className="align-middle">{this.props.dob.age} </td>
        <td className="align-middle">
          {this.props.location.state}, {this.props.location.country}{" "}
        </td>
        <td className="align-middle">{this.props.login.username} </td>
        <td className="align-middle">{this.props.email} </td>
        <td className="align-middle">{this.props.phone} </td>
      </tr>
    );
  }
}
